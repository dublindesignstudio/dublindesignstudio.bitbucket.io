package com.getscriba.sampleapp.android;

import android.graphics.Path;

/**
 * Created by alannaogrady on 26/04/2018.
 */

public class ScribaPath {
    public int color;
    public int strokeWidth;
    public Path path;

    public ScribaPath(int color, int strokeWidth, Path path) {
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.path = path;
    }
}
