package com.getscriba.sampleapp.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.getscriba.sdk.android.scribasdk.Buzz;
import com.getscriba.sdk.android.scribasdk.ClickType;
import com.getscriba.sdk.android.scribasdk.ScribaStylusDevice;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManager;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManagerCallbacks;

import java.util.List;



/**
 * Created by alannaogrady on 12/03/2018.
 */

public class ClickBuzzDemoActivity extends AppCompatActivity implements ScribaStylusManagerCallbacks {
    private final static String TAG = ClickBuzzDemoActivity.class.getName();

    private TextView mClickLabel;
    private TextView mSingleClickTextView;
    private TextView mDoubleClickTextView;
    private TextView mTripleClickTextView;
    private ScribaStylusManager mManager;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_buzz_demo);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.toolbar, null);
        TextView mainTitle = actionBarLayout.findViewById(R.id.toolbar_title);
        ImageView backButton = actionBarLayout.findViewById(R.id.back_button_toolbar);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView backText = actionBarLayout.findViewById(R.id.back_icon_text);
        backText.setText(R.string.back_button);

        mainTitle.setText(R.string.click_buzz_demo_title);
        getSupportActionBar().setCustomView(actionBarLayout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        mClickLabel = findViewById(R.id.click_label);
        mSingleClickTextView = findViewById(R.id.single_click_textview);
        mDoubleClickTextView = findViewById(R.id.double_click_textview);
        mTripleClickTextView = findViewById(R.id.triple_click_textview);
        Button buzzButton = findViewById(R.id.buzz_button);

        mManager = ScribaStylusManager.getInstance(this);
        mManager.addCallbackInterface(this);

        buzzButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click-buzz-demo buzz");
                mManager.hapticsBuzz(Buzz.SINGLE_BUZZ);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mManager.enableHapticsBuzz(true);
        mManager.enableSmartLock(false);
        mManager.enableSqueezeZoneChangeBuzz(false);
    }

    @Override
    public void onStart() {
        defaultUI();
        super.onStart();
    }

    @Override
    public void onDestroy() {
        mManager.removeCallbackInterface(this);
        super.onDestroy();
    }

    @Override
    public void foundDevices(List<ScribaStylusDevice> devices) {

    }

    @Override
    public void connectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void disconnectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void updateBatteryStateOfDevice(ScribaStylusDevice device, int batteryState) {

    }

    @Override
    public void changeDepressionForDevice(ScribaStylusDevice device, float depression) {

    }

    @Override
    public void changeSqueezeZoneForDevice(ScribaStylusDevice device, int squeezeZone) {

    }

    @Override
    public void clickWithDevice(ScribaStylusDevice device, ClickType clickType) {
        runOnUiThread(() -> {
            if (clickType == ClickType.SINGLE) {
                mHandler.removeCallbacks(resetRunnable);
                mClickLabel.setText(R.string.single_click_label);
                mSingleClickTextView.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mDoubleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mTripleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mHandler.postDelayed(resetRunnable, 1000);
            }
            else if (clickType == ClickType.DOUBLE) {
                mHandler.removeCallbacks(resetRunnable);
                mClickLabel.setText(R.string.double_click_label);
                mSingleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mDoubleClickTextView.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mTripleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mHandler.postDelayed(resetRunnable, 1000);
            }
            else {
                mHandler.removeCallbacks(resetRunnable);
                mClickLabel.setText(R.string.triple_click_label);
                mSingleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mDoubleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mTripleClickTextView.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mHandler.postDelayed(resetRunnable, 1000);
            }
        });
    }

    @Override
    public void enabledLockConditionChange(boolean enabled) {

    }

    @Override
    public void lockStateChange(boolean lockOn) {

    }

    @Override
    public void brushSizeChange(float brushSize) {

    }

    private Runnable resetRunnable = new Runnable() {
        @Override
        public void run() {
            defaultUI();
        }
    };

    private void defaultUI() {
        mClickLabel.setText(R.string.click_now_label);
        mSingleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
        mDoubleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
        mTripleClickTextView.setBackgroundColor(getResources().getColor(R.color.grey_squares));
    }
}
