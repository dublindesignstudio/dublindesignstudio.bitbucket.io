package com.getscriba.sampleapp.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by alannaogrady on 26/04/2018.
 */

public class ScribaDrawingView extends View {

    public static int DEFAULT_BRUSH_SIZE = 1;
    public static final int DEFAULT_COLOR = Color.RED;
    public static final int DEFAULT_BG_COLOR = Color.WHITE;
    private static final float TOUCH_TOLERANCE = 4;
    private float mX, mY;
    private Path mPath;
    private Paint mPaint;
    private ArrayList<ScribaPath> paths = new ArrayList<>();
    private int currentColor;
    private int backgroundColor = DEFAULT_BG_COLOR;
    private int strokeWidth;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);
    private ScribaPath mCurrentScribaPath;
    private int mCurrentStrokeWidth;

    public ScribaDrawingView(Context context) {
        this(context, null);
    }

    public ScribaDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(DEFAULT_COLOR);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setXfermode(null);
        mPaint.setAlpha(0xff);

    }

    public void init(DisplayMetrics metrics) {
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        currentColor = DEFAULT_COLOR;
        strokeWidth = DEFAULT_BRUSH_SIZE;
    }


    public void setBrushSize(float brushSize) {
        strokeWidth = Math.round(brushSize);
        invalidate();
    }


    public void clear() {
        backgroundColor = DEFAULT_BG_COLOR;
        paths.clear();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        mCanvas.drawColor(backgroundColor);

        for (ScribaPath scribaPath : paths) {

            mPaint.setColor(scribaPath.color);
            mPaint.setStrokeWidth((scribaPath.strokeWidth));
            mPaint.setMaskFilter(null);

            mCanvas.drawPath(scribaPath.path, mPaint);

        }

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.restore();
    }

    private void touchStart(float x, float y) {

        mPath = new Path();
        mCurrentScribaPath = new ScribaPath(currentColor, strokeWidth, mPath);
        paths.add(mCurrentScribaPath);
        mCurrentStrokeWidth = strokeWidth;

        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;

    }

    private void touchMove(float x, float y) {

        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (strokeWidth == mCurrentStrokeWidth) {

            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }

        } else {

            PathMeasure pm = new PathMeasure(mPath, false);
            float pathEndCoordinates[] = {0f, 0f};
            pm.getPosTan(pm.getLength(), pathEndCoordinates, null);

            if (pathEndCoordinates[0] == 0) {
                // First path, typically from starting to draw with the Scriba under pressure
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                newScribaPath(x, y);

            } else {
                mPath = new Path();
                mPath.moveTo(pathEndCoordinates[0], pathEndCoordinates[1]);
                mPath.moveTo(pathEndCoordinates[0], pathEndCoordinates[1]);
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                newScribaPath(x, y);
            }


        }

    }

    private void newScribaPath(float x, float y) {
        mX = x;
        mY = y;
        mCurrentScribaPath = new ScribaPath(currentColor, strokeWidth, mPath);
        paths.add(mCurrentScribaPath);
        mCurrentStrokeWidth = strokeWidth;
    }

    private void touchUp() {
        mPath.lineTo(mX, mY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touchUp();
                invalidate();
                break;
        }

        return true;
    }
}
