package com.getscriba.sampleapp.android;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.getscriba.sdk.android.scribasdk.Buzz;
import com.getscriba.sdk.android.scribasdk.ClickType;
import com.getscriba.sdk.android.scribasdk.ScribaStylusDevice;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManager;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManagerCallbacks;

import java.util.List;



/**
 * Created by alannaogrady on 06/04/2018.
 */

public class SmartLockActivity extends AppCompatActivity implements ScribaStylusManagerCallbacks {

    private static final String TAG = SmartLockActivity.class.getName();

    private ScribaStylusManager mManager;
    private TextView mLockInstructions;
    private TextView mPressureValue;
    private ImageView mLockImageView;
    private TextView mSmartLockLabel;
    private boolean mLockOn = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_lock);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.toolbar, null);
        TextView mainTitle = actionBarLayout.findViewById(R.id.toolbar_title);
        ImageView backButton = actionBarLayout.findViewById(R.id.back_button_toolbar);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView backText = actionBarLayout.findViewById(R.id.back_icon_text);
        backText.setText(R.string.back_button);

        mainTitle.setText(R.string.smart_lock_title);
        getSupportActionBar().setCustomView(actionBarLayout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        mLockInstructions = findViewById(R.id.lock_instructions);
        mPressureValue = findViewById(R.id.lock_pressure_value);
        mLockImageView = findViewById(R.id.lock_image_view);
        mSmartLockLabel = findViewById(R.id.lock_label);

        mLockImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mManager.isSmartLockEnabled()) {
                    //disable lock
                    mManager.enableSmartLock(false);
                    mLockImageView.setBackgroundColor(Color.TRANSPARENT);
                    mSmartLockLabel.setText(R.string.lock_deactivated);
                    mLockInstructions.setText(R.string.lock_deactivated_instructions);
                }
                else {
                    //enable lock
                    mManager.enableSmartLock(true);
                    //if was locked before it was disabled
                    if (mManager.isSmartLockOn()) {
                        mLockImageView.setBackgroundResource(R.drawable.red_circle);
                        mLockInstructions.setText(R.string.lock_engaged_instructions);
                        mSmartLockLabel.setText(R.string.lock_engaged);
                    }
                    else {
                        mLockImageView.setBackgroundResource(R.drawable.circle_background);
                        mSmartLockLabel.setText(R.string.lock_enabled);
                        mLockInstructions.setText(R.string.smart_lock_instructions);
                    }
                }

            }
        });


        mManager = ScribaStylusManager.getInstance(this);
        mManager.addCallbackInterface(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        mManager.enableHapticsBuzz(true);
        mManager.enableSmartLock(true);
        mManager.enableSqueezeZoneChangeBuzz(false);
    }

    @Override
    public void onDestroy(){
        mManager.removeCallbackInterface(this);
        super.onDestroy();
    }

    @Override
    public void foundDevices(List<ScribaStylusDevice> devices) {

    }

    @Override
    public void connectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void disconnectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void updateBatteryStateOfDevice(ScribaStylusDevice device, int batteryState) {

    }

    @Override
    public void changeDepressionForDevice(ScribaStylusDevice device, float depression) {
        runOnUiThread(() -> {
            if (mManager.isSmartLockEnabled() && !mLockOn){
                mPressureValue.setText(getString(R.string.lock_pressure_value, depression));
            }
        });
    }

    @Override
    public void changeSqueezeZoneForDevice(ScribaStylusDevice device, int squeezeZone) {

    }

    @Override
    public void clickWithDevice(ScribaStylusDevice device, ClickType clickType) {

    }

    @Override
    public void enabledLockConditionChange(boolean enabled) {

    }

    @Override
    public void lockStateChange(boolean lockOn) {
        runOnUiThread(() -> {
            if (lockOn) {
                mLockOn = true;
                mLockImageView.setBackgroundResource(R.drawable.red_circle);
                mLockImageView.setImageResource(R.drawable.closed_lock);
                mLockInstructions.setText(R.string.lock_engaged_instructions);
                mSmartLockLabel.setText(R.string.lock_engaged);
                mManager.hapticsBuzz(Buzz.SINGLE_BUZZ);
            }
            else {
                mLockOn = false;
                mLockImageView.setBackgroundResource(R.drawable.circle_background);
                mLockImageView.setImageResource(R.drawable.open_active_lock);
                mLockInstructions.setText(R.string.smart_lock_instructions);
                mSmartLockLabel.setText(R.string.lock_enabled);
            }
        });
    }

    @Override
    public void brushSizeChange(float brushSize) {

    }
}
