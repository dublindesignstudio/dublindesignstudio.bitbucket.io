package com.getscriba.sampleapp.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.getscriba.sdk.android.scribasdk.ClickType;
import com.getscriba.sdk.android.scribasdk.ScribaStylusDevice;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManager;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManagerCallbacks;

import java.util.List;




public class InitialDemoActivity extends AppCompatActivity implements ScribaStylusManagerCallbacks, View.OnClickListener {

    private static final String TAG = InitialDemoActivity.class.getName();

    private TextView mDeviceName;
    private TextView mBatteryValue;
    private ScribaStylusManager mManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_demo);


        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.toolbar, null);
        TextView mainTitle = actionBarLayout.findViewById(R.id.toolbar_title);
        ImageView backButton = actionBarLayout.findViewById(R.id.back_button_toolbar);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView backText = actionBarLayout.findViewById(R.id.back_icon_text);
        backText.setText(R.string.scriba_demo);
        mainTitle.setText(R.string.initial_demo_title);
        getSupportActionBar().setCustomView(actionBarLayout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);


        mDeviceName = findViewById(R.id.scriba_device_name);
        mBatteryValue = findViewById(R.id.battery_textview);
        Button clickBuzzDemo = findViewById(R.id.click_buzz_demo_button);
        Button squeezeZoneDemo = findViewById(R.id.squeeze_zone_demo_button);
        Button smartLockDemo = findViewById(R.id.smartlock_demo_button);
        Button brushesDemo = findViewById(R.id.brushes_demo_button);
        clickBuzzDemo.setOnClickListener(this);
        squeezeZoneDemo.setOnClickListener(this);
        smartLockDemo.setOnClickListener(this);
        brushesDemo.setOnClickListener(this);

        if (savedInstanceState != null) {
            String batteryValueString = savedInstanceState.getString("batteryValue");
            mBatteryValue.setText(batteryValueString);
            String deviceName = savedInstanceState.getString("deviceName");
            mDeviceName.setText(deviceName);
        }

        mManager = ScribaStylusManager.getInstance(this);
        mManager.addCallbackInterface(this);

        updateDeviceInfo();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("batteryValue", mBatteryValue.getText().toString());
        outState.putString("deviceName", mDeviceName.getText().toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        mManager.enableHapticsBuzz(false);
        mManager.enableSmartLock(false);
        mManager.enableSqueezeZoneChangeBuzz(false);
    }

    @Override
    public void onDestroy(){
        mManager.removeCallbackInterface(this);
        super.onDestroy();
    }

    @Override
    public void foundDevices(List<ScribaStylusDevice> devices) {

    }

    @Override
    public void connectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void disconnectedDevice(ScribaStylusDevice device) {
        runOnUiThread(() -> {
            if (mBatteryValue != null && mDeviceName != null) {
                mDeviceName.setText(R.string.default_device_name);
                mBatteryValue.setText(R.string.not_available);
            }
        });
    }

    @Override
    public void updateBatteryStateOfDevice(ScribaStylusDevice device, int batteryState) {
        runOnUiThread(() -> {
            if (mBatteryValue != null && mDeviceName != null) {
                mDeviceName.setText(device.name());
                mBatteryValue.setText(getString(R.string.battery, batteryState));
            }
        });
    }

    @Override
    public void changeDepressionForDevice(ScribaStylusDevice device, float depression) {

    }

    @Override
    public void changeSqueezeZoneForDevice(ScribaStylusDevice device, int squeezeZone) {

    }

    @Override
    public void clickWithDevice(ScribaStylusDevice device, ClickType clickType) {

    }

    @Override
    public void enabledLockConditionChange(boolean enabled) {

    }

    @Override
    public void lockStateChange(boolean lockOn) {

    }

    @Override
    public void brushSizeChange(float brushSize) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.click_buzz_demo_button:
                startActivity(new Intent(InitialDemoActivity.this, ClickBuzzDemoActivity.class));
                break;
            case R.id.squeeze_zone_demo_button:
                startActivity(new Intent(InitialDemoActivity.this, SqueezeZoneActivity.class));
                break;
            case R.id.smartlock_demo_button:
                startActivity(new Intent(InitialDemoActivity.this, SmartLockActivity.class));
                break;
            case R.id.brushes_demo_button:
                startActivity(new Intent(InitialDemoActivity.this, BrushesActivity.class));
                break;
            default:
                break;
        }
    }

    private void updateDeviceInfo() {
        if (mDeviceName.getText().equals("Scriba device")){
            mDeviceName.setText(mManager.mConnectedDevice.name());
        }
        if (mBatteryValue.getText().equals("n/a") && (mManager.mCurrentDeviceBatteryValue >= 0)){
            mBatteryValue.setText(getString(R.string.battery, mManager.mCurrentDeviceBatteryValue));
        }
    }
}
