package com.getscriba.sampleapp.android;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.getscriba.sdk.android.scribasdk.ClickType;
import com.getscriba.sdk.android.scribasdk.ScribaStylusDevice;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManager;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManagerCallbacks;

import java.util.List;

/**
 * Created by alannaogrady on 27/04/2018.
 */

public class BrushesActivity extends AppCompatActivity implements ScribaStylusManagerCallbacks {
    private static final String TAG = BrushesActivity.class.getName();

    private ScribaStylusManager mManager;
    private ScribaDrawingView paintView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brushes);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.toolbar, null);
        TextView mainTitle = actionBarLayout.findViewById(R.id.toolbar_title);
        ImageView backButton = actionBarLayout.findViewById(R.id.back_button_toolbar);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView backText = actionBarLayout.findViewById(R.id.back_icon_text);
        backText.setText(R.string.back_button);
        mainTitle.setText(R.string.brushes_demo_title);
        getSupportActionBar().setCustomView(actionBarLayout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        paintView = (ScribaDrawingView) findViewById(R.id.paintView);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintView.init(metrics);

        mManager = ScribaStylusManager.getInstance(this);
        mManager.addCallbackInterface(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.clear:
                paintView.clear();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mManager.enableHapticsBuzz(false);
        mManager.enableSmartLock(false);
        mManager.enableSqueezeZoneChangeBuzz(false);
    }

    @Override
    public void onDestroy(){
        mManager.removeCallbackInterface(this);
        super.onDestroy();
    }

    @Override
    public void foundDevices(List<ScribaStylusDevice> list) {

    }

    @Override
    public void connectedDevice(ScribaStylusDevice scribaStylusDevice) {

    }

    @Override
    public void disconnectedDevice(ScribaStylusDevice scribaStylusDevice) {

    }

    @Override
    public void updateBatteryStateOfDevice(ScribaStylusDevice scribaStylusDevice, int i) {

    }

    @Override
    public void changeDepressionForDevice(ScribaStylusDevice scribaStylusDevice, float v) {

    }

    @Override
    public void changeSqueezeZoneForDevice(ScribaStylusDevice scribaStylusDevice, int i) {

    }

//    @Override
//    public void singleClickWithDevice(ScribaStylusDevice scribaStylusDevice) {
//        runOnUiThread(() -> {
//            mManager.removeSmartLock();
//            mManager.enableSmartLock(false);
//        });
//    }

//    @Override
//    public void doubleClickWithDevice(ScribaStylusDevice scribaStylusDevice) {
//        runOnUiThread(() -> {
//            mManager.lockBrushSize(50);
//        });
//    }

    @Override
    public void clickWithDevice(ScribaStylusDevice device, ClickType clickType) {
        runOnUiThread(() -> {
            if (clickType == ClickType.SINGLE) {
                mManager.removeSmartLock();
                mManager.enableSmartLock(false);
            }
            else if (clickType == ClickType.DOUBLE) {
                mManager.lockBrushSize(50);
            }
            else  {
                paintView.clear();
            }
        });

    }

    @Override
    public void enabledLockConditionChange(boolean b) {

    }

    @Override
    public void lockStateChange(boolean b) {

    }

    @Override
    public void brushSizeChange(float v) {
        runOnUiThread(() -> {
            paintView.setBrushSize(v);
        });
    }
}
