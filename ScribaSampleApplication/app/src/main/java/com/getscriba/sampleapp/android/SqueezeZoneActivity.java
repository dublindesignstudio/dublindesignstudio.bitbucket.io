package com.getscriba.sampleapp.android;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.getscriba.sdk.android.scribasdk.ClickType;
import com.getscriba.sdk.android.scribasdk.ScribaStylusDevice;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManager;
import com.getscriba.sdk.android.scribasdk.ScribaStylusManagerCallbacks;

import java.util.List;


/**
 * Created by alannaogrady on 04/04/2018.
 */

public class SqueezeZoneActivity extends AppCompatActivity implements ScribaStylusManagerCallbacks {

    private ScribaStylusManager mManager;
    private TextView mZoneValue;
    private TextView mZone0;
    private TextView mZone1;
    private TextView mZone2;
    private TextView mZone3;
    private TextView mZone4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squeeze_demo);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.toolbar, null);
        TextView mainTitle = actionBarLayout.findViewById(R.id.toolbar_title);
        ImageView backButton = actionBarLayout.findViewById(R.id.back_button_toolbar);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView backText = actionBarLayout.findViewById(R.id.back_icon_text);
        backText.setText(R.string.back_button);

        mainTitle.setText(R.string.squeeze_zone_demo);
        getSupportActionBar().setCustomView(actionBarLayout);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        mZoneValue = findViewById(R.id.squeeze_zone_value);
        mZone0 = findViewById(R.id.zone_0);
        mZone1 = findViewById(R.id.zone_1);
        mZone2 = findViewById(R.id.zone_2);
        mZone3 = findViewById(R.id.zone_3);
        mZone4 = findViewById(R.id.zone_4);

        mManager = ScribaStylusManager.getInstance(this);
        mManager.addCallbackInterface(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        mManager.enableHapticsBuzz(false);
        mManager.enableSmartLock(false);
        mManager.enableSqueezeZoneChangeBuzz(false);
    }

    @Override
    public void onDestroy(){
        mManager.removeCallbackInterface(this);
        super.onDestroy();
    }

    @Override
    public void foundDevices(List<ScribaStylusDevice> devices) {

    }

    @Override
    public void connectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void disconnectedDevice(ScribaStylusDevice device) {

    }

    @Override
    public void updateBatteryStateOfDevice(ScribaStylusDevice device, int batteryState) {

    }

    @Override
    public void changeDepressionForDevice(ScribaStylusDevice device, float depression) {
        runOnUiThread(() -> {
            mZoneValue.setText(getString(R.string.squeeze_zone_value, depression));
        });
    }

    @Override
    public void changeSqueezeZoneForDevice(ScribaStylusDevice device, int squeezeZone) {
        runOnUiThread(() -> {
            if (squeezeZone == 0){
                mZone0.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mZone1.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone2.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone3.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone4.setBackgroundColor(getResources().getColor(R.color.grey_squares));
            }
            else if (squeezeZone == 1) {
                mZone0.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone1.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mZone2.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone3.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone4.setBackgroundColor(getResources().getColor(R.color.grey_squares));
            }
            else if (squeezeZone == 2) {
                mZone0.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone1.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone2.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mZone3.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone4.setBackgroundColor(getResources().getColor(R.color.grey_squares));
            }
            else if (squeezeZone == 3) {
                mZone0.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone1.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone2.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone3.setBackgroundColor(getResources().getColor(R.color.blue_squares));
                mZone4.setBackgroundColor(getResources().getColor(R.color.grey_squares));
            }
            else {
                mZone0.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone1.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone2.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone3.setBackgroundColor(getResources().getColor(R.color.grey_squares));
                mZone4.setBackgroundColor(getResources().getColor(R.color.blue_squares));
            }
        });

    }

    @Override
    public void clickWithDevice(ScribaStylusDevice device, ClickType clickType) {

    }

    @Override
    public void enabledLockConditionChange(boolean enabled) {

    }

    @Override
    public void lockStateChange(boolean lockOn) {

    }

    @Override
    public void brushSizeChange(float brushSize) {

    }
}
